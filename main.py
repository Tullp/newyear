
import os
import datetime
import traceback
import telebot
from PIL import Image, ImageDraw, ImageFont
from user_statistics import Statistics

token = '794139176:AAFnEHAmHNlIfWBVvVoJF2DPA95Ot7TFAJw'
tullp = 165647468

bot = telebot.TeleBot(token, parse_mode="HTML")
statistic = Statistics(bot, [tullp], config={"notify_about_new_users": True})


def get_timedata():
	new_year = datetime.datetime(datetime.datetime.now().year + 1, 1, 1).timestamp()
	now = datetime.datetime.now().timestamp()
	delta = int(new_year - now)
	return {
		"days": delta // (24*3600),
		"hours": delta % (24*3600) // 3600,
		"min": delta % 3600 // 60,
		"sec": delta % 60
	}


def get_baltika_image(timedata):
	timedata = {segment: str(value) for segment, value in timedata.items()}
	numbers = list("".join(timedata.values()))
	images = {num: Image.open(os.path.join("baltika", f"b{num}.png")) for num in set(numbers)}
	segments = {segment: Image.open(os.path.join("baltika", f"{segment}.png")) for segment in timedata.keys()}
	width = sum(images[num].width for num in numbers) + (len(numbers) - 1) * 20 + (len(timedata) - 1) * 120
	height = max(img.height for img in images.values()) + 120
	image = Image.new('RGB', (width, height), "white")
	axis_x = 20
	for i, (segment, value) in enumerate(timedata.items(), 1):
		start_axis_x = axis_x
		for num in value:
			image.paste(images[num], (axis_x, 20))
			axis_x += images[num].width
		segment_width = start_axis_x + ((axis_x - start_axis_x) // 2) - (segments[segment].width // 2)
		segment_height = height - 80
		image.paste(segments[segment], (segment_width, segment_height))
		axis_x += 120
	image_path = os.path.join("bdate", "{days}_{hours}_{min}.png".format(**timedata))
	image.save(image_path)
	return image_path


def debug(func):
	def safe_func(*args, **kwargs):
		try:
			func(*args, **kwargs)
		except Exception:
			bot.send_message(tullp, str(traceback.format_exc()))
	return safe_func


@bot.message_handler(commands=["start"])
@debug
@statistic.track
def start_handler(message):
	bot.send_message(message.chat.id, "Happy New Year!")


@bot.message_handler(commands=["new_year"])
@debug
@statistic.track
def new_year_handler(message):
	timedata = get_timedata()
	bot.send_message(message.chat.id, f"{timedata['days']}d {timedata['hours']}h {timedata['min']}m")


@bot.message_handler(commands=["baltika"])
@debug
@statistic.track
def baltika_handler(message):
	timedata = get_timedata()
	image_path = get_baltika_image(timedata)
	if not os.path.exists(image_path):
		return
	with open(image_path, "rb") as file:
		bot.send_photo(message.chat.id, file.read(), caption=f"{timedata['days']}d {timedata['hours']}h {timedata['min']}m")
	if os.path.exists(image_path):
		os.remove(image_path)


@bot.message_handler(commands=["eval"], func=lambda msg: msg.chat.id == tullp)
@debug
def eval_handler(message):
	bot.send_message(tullp, str(eval(message.text[6:])))


@bot.message_handler(commands=["exec"], func=lambda msg: msg.chat.id == tullp)
@debug
def exec_handler(message):
	exec(message.text[6:])


if __name__ == "__main__":
	bot.infinity_polling(skip_pending=True)
