
import os
import json
from datetime import datetime
from math import ceil
from string import ascii_lowercase
from random import choices


def uuid():
	return "-".join("".join(choices(ascii_lowercase, k=5)) for i in range(4))


class Manager(list):

	def __init__(self, path, token="_id"):
		super().__init__()
		self.path = path
		self.token = token
		if not os.path.exists(path):
			os.makedirs(path)
		self.load()

	def load(self):
		for fn in os.listdir(self.path):
			with open(os.path.join(self.path, fn), "r", encoding="utf-8") as f:
				super().append(json.load(f))

	def append(self, item):
		item["_id"] = uuid()
		super().append(item)
		self.save(item)
		return item

	def delete(self, item):
		os.remove(os.path.join(self.path, f"{item[self.token]}.json"))
		super().remove(item)
		return item

	def find(self, options):
		return [item for item in self if all(item.get(key) == value for key, value in options.items())]

	def findone(self, options):
		for item in self:
			if all(item.get(key) == value for key, value in options.items()):
				return item
		return None

	def update(self, item, data):
		item.update(data)
		self.save(item)
		return item

	def save(self, item):
		with open(os.path.join(self.path, f"{item[self.token]}.json"), "w", encoding="utf-8") as f:
			json.dump(item, f, indent=4, ensure_ascii=False)


class Statistics:

	def __init__(self, bot, admins, config=None):
		if config is None:
			config = {}
		self.bot = bot
		self.admins = admins
		self.config = config
		self.UserManager = Manager(os.path.join(os.getcwd(), "users"), "chat_id")
		self.register_handlers()

	def track(self, handler):
		def tracked_handler(message):
			dt = datetime.now().date().__str__()
			doc = self.UserManager.findone({"chat_id": message.chat.id})
			if doc is None:
				doc = self.UserManager.append({
					"chat_id": message.chat.id,
					"username": f"@{message.chat.username} {message.chat.title}",
					"activity": dt
				})
				if self.config.get("notify_about_new_users", False):
					for user_id in self.admins:
						self.bot.send_message(user_id, f"New user!\n{doc['username']} ({doc['chat_id']})")
			elif doc["activity"] != dt:
				self.UserManager.update(doc, {"activity": dt})
			handler(message)
		return tracked_handler

	def register_handlers(self):
		self.bot.register_message_handler(
			self.get_users,
			commands=["users"],
			func=lambda msg: msg.chat.id in self.admins
		)

	def get_users(self, message):
		self.bot.send_message(message.from_user.id, f"<b>Users ({len(self.UserManager)}):</b>")
		for i in range(ceil(len(self.UserManager) / 50)):
			self.bot.send_message(message.from_user.id, "\n".join(
				f"<a href='tg://user?id={user.get('chat_id', 0)}'>{user.get('username')}</a> ({user.get('activity')})"
				for user in self.UserManager[i * 50: (i + 1) * 50]
			))
